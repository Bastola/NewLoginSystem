<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
// use App\Http\Controllers\Controller;
use App\Model\AboutUs;

class AboutUsController extends Controller
{
  public function index()
  {
    $icons = AboutUs::get();
    return view('aboutus',compact('icons'));
  }
  public function upload(Request $request)
   {
     $this->validate($request, [

           'icon' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
           'title' => 'required',
           'description'=>'required',
       ]);

       $input['icon'] = time().'.'.$request->icon->getClientOriginalExtension();
       $request->icon->move(public_path('icons'), $input['icon']);
       $input['title'] = $request->title;
       $input['description'] = $request->description;

       AboutUs::create($input);
     return back()
       ->with('success','Icon Uploaded successfully.');
   }


   /**
    * Remove Image function
    *
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
     AboutUs::find($id)->delete();
     return back()
       ->with('success','Icon removed successfully.');
   }

}
