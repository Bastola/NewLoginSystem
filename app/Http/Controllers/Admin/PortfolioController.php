<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Portfolio;

class PortfolioController extends Controller
{
  public function index()
  {
    $portfolio_images = Portfolio::get();
    return view('admin.contents.dashboard.portfolio',compact('portfolio_images'));



  }
  public function upload(Request $request)
   {
     $this->validate($request, [

           'portfolio_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
           'portfolio_title' => 'required',
           'portfolio_span_desc'=>'required',
       ]);

       $input['portfolio_image'] = time().'.'.$request->portfolio_image->getClientOriginalExtension();
       $request->portfolio_image->move(public_path('/assets/admin/assets/images/portfolio_images'), $input['portfolio_image']);
       $input['portfolio_title'] = $request->portfolio_title;
       $input['portfolio_span_desc'] = $request->portfolio_span_desc;

       Portfolio::create($input);
       return back()
       ->with('success','Portfolio Image Uploaded successfully.');
   }


   /**
    * Remove Image function
    *
    * @return \Illuminate\Http\Response

    */
   public function destroy($id)
   {
     Portfolio::find($id)->delete();
     return back()
       ->with('success','Portfolio Images removed successfully.');
   }



}
