<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Team;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;

class TeamController extends Controller
{
  public function addTeam(Request $request)
  {
      $rules = array(
              'team_pic'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
              'team_title' => 'required',
              'team_span_desc'=>'required',
      );


          $validator = Validator::make(Input::all(), $rules);

      if ($validator->fails()) {
          return Response::json(array(

                  'errors' => $validator->getMessageBag()->toArray(),
          ));
      } else {
          $team = new Team();
          $team->team_pic = time().'.'.$request->team_pic->getClientOriginalExtension();
          $request->team_pic->move(public_path('/assets/admin/assets/images/team_pic'), $team->team_pic);
          $team->team_title = $request->team_title;
          $team->team_span_desc= $request->team_span_desc;

          $team->save();
          return response()->json($team);
      }
  }
  public function readTeam(Request $req)
   {
       $team = Team::all();

       return view('admin.contents.dashboard.team')->withData($team);
   }
   public function editTeam(Request $req)
   {
       $team = Team::find($req->team_id);
       $team->team_title = $req->team_title;
       $team->save();

       return response()->json($team);
   }
   public function deleteTeam(Request $req)
   {
       Team::find($req->team_id)->delete();

       return response()->json();
   }
}
