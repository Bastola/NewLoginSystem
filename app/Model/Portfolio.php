<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
  protected $table='portfolio';
  protected $fillable=['portfolio_image','portfolio_title','portfolio_span_desc'];

}
