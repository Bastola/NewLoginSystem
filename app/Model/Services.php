<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
  protected $table='services';
  protected $fillable=['services_icon','services_title','services_description'];
}
