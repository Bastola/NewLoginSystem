<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
  protected $table='team';
  protected $fillable=['team_pic','team_title','team_span_desc'];
}
