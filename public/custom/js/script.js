$(document).ready(function() {

  $(document).on('click', '#add', function() {
          $('modal-title').text('Add');
          $('#addModal').modal('show');
    });

  $(document).on('click','.edit-modal', function(){
    $('modal-title').text('Edit');
    $('#editModal').modal('show');
  });

  $(document).on('click','#delete', function(){
    $('modal-title').text('Delete');
    $('#deleteModal').modal('show');
  });
});
