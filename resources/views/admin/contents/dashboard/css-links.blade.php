<!-- Favicon -->
<link rel="shortcut icon" href="{{asset('assets/admin/assets/images/favicon.ico')}}">

<!-- Bootstrap CSS -->
<link href="{{asset('assets/admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />

<!-- Font Awesome CSS -->
<link href="{{asset('assets/admin/assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />

<!-- Custom CSS -->
<link href="{{asset('assets/admin/assets/css/style.css')}}" rel="stylesheet" type="text/css" />

<!-- BEGIN CSS for this page -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>
<!-- END CSS for this page -->

  <style type="text/css">
.footer {
border-top: 1px solid rgba(115, 140, 152, 0.2);
bottom: 0;
color: #818a91;
text-align: left !important;
padding: 10px;
position: absolute;
right: 0;
left: 250px;
background-color: #343a40;
</style>
