@extends('admin.layouts.admin_masterpage')

@section('title')Dashboard
@endsection

@section('css-links')
 @include('admin.contents.dashboard.css-links')

@endsection

@section('content')

<div class="container-fluid">

      <div class="row">
            <div class="col-xl-12">
                <div class="breadcrumb-holder">
                    <h1 class="main-title float-left">Dashboard</h1>
                    <ol class="breadcrumb float-right">
                      <li class="breadcrumb-item">Home</li>
                      <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
      </div>
  </div>
  <div>
<h1>Hello Admin!!! See on the left side to make changes to your website>>></h1>
  </div>


      @endsection

      @section('js-links')
      @include('admin.contents.dashboard.js-links')
      @endsection
