@extends('admin.layouts.admin_masterpage')

@section('title') Portfolio Edits
@endsection

@section('css-links')

 @include('admin.contents.dashboard.css-links')
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <!-- References: https://github.com/fancyapps/fancyBox -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>


  <style type="text/css">
  .gallery
  {
      display: inline-block;
      margin-top: 20px;
  }
  .close-icon{
    border-radius: 50%;
      position: absolute;
      right: 5px;
      top: -10px;
      padding: 5px 8px;
  }
  .form-image-upload{
      background: #e8e8e8 none repeat scroll 0 0;
      padding: 15px;
  }
  </style>

@endsection

@section('content')

<div class="container-fluid">

      <div class="row">
            <div class="col-xl-12">
                <div class="breadcrumb-holder">
                    <h1 class="main-title float-left">Dashboard</h1>
                    <ol class="breadcrumb float-right">
                      <li class="breadcrumb-item">Home</li>
                      <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
      </div>


      <div class="panel">
    <h3>Portfolio CRUD Example</h3>
    <form action="{{ url('admin/portfolio') }}" class="form-portfolio-upload" method="POST" enctype="multipart/form-data">


        {!! csrf_field() !!}


        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
        </div>
        @endif

        <form>
        <div class="jumbotron text-center">
        <div class="row">
            <div class="col-md-7">
                <strong>Image:</strong>
                <input type="file" name="portfolio_image" class="form-control">
          </div>
            <div class="col-md-7">
                <strong>Title:</strong>
                <input type="text" name="portfolio_title" class="form-control" placeholder="Title">
            </div>
            <div class="col-md-7">
                <strong>Description:</strong>
                <input type="text" name="portfolio_span_desc" class="form-control" placeholder="Description">
            </div>
        </div>
        <br/>

      </br/>

        <div class="container">

          <input type="submit" class="btn btn-success"/>
          <input type="reset" class="btn btn-primary" value="Reset" />

        </div>

      </div>
    </form>


    <div class="row">
    <div class='list-group portfolio_gallery'>


            @if($portfolio_images->count())
                @foreach($portfolio_images as $portfolio_image)
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="/assets/admin/assets/images/{{ $portfolio_image->portfolio_image }}">
                        <img class="img-responsive" alt="" src="/assets/admin/assets/images/{{ $portfolio_image->portfolio_image }}" />
                        <div class='text-center'>
                            <small class='text-muted'>{{ $portfolio_image->portfolio_title }}</small>
                        </div> <!-- text-center / end -->
                    </a>
                    <form action="{{ url('admin/portfolio',$portfolio_image->id) }}" method="POST">
                    <input type="hidden" name="_method" value="delete">
                    {!! csrf_field() !!}
                    <button type="submit" class="close-icon btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button>
                    </form>
                </div> <!-- col-6 / end -->
                @endforeach
            @endif


        </div> <!-- list-group / end -->
    </div> <!-- row / end -->
</div> <!-- container / end -->
  </div>


      @endsection

      @section('js-links')
      @include('admin.contents.dashboard.js-links')
      <script type="text/javascript">
    $(document).ready(function(){
        $(".fancybox").fancybox({
            openEffect: "none",
            closeEffect: "none"
        });
    });
</script>
      @endsection
