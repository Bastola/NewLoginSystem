@extends('admin.layouts.admin_masterpage')

@section('title')
  Manage Team
@endsection

@section('css-links')
  @include('admin.contents.dashboard.css-links')
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
@endsection

@section('content')
  <div class="form-group row add">
    <div class="col-md-8">
      <div class="col-md-4">
        <button class="add-modal" type="submit" id="add">
          <span class="glyphicon glyphicon-plus"></span> ADD
        </button>
      </div>
    </div>
  </div>

  {{ csrf_field() }}
  <div class="table-responsive text-center">
    <table class="table table-borderless" id="table">
      <thead>
        <tr>
          <th class="text-center">S.N.</th>
          <th class="text-center">Team Picture</th>
          <th class="text-center">Team Title</th>
          <th class="text-center">Team Description</th>
        </tr>
      </thead>
      @foreach($data as $team)
        <tr class="team{{$team->team_id}}">
          <td>{{$team->team_id}}</td>
          <td>{{$team->team_pic}}</td>
          <td>{{$team->team_title}}</td>
          <td>{{$team->team_span_desc}}</td>
          <td><button class="edit-modal btn btn-info" data-team_id="{{$team->team_id}}"
            data-team_pic="{{$team->team_pic}}" data-team_title="{{$team->team_title}}" data-team_span_desc="{{$team->team_span_desc}}">
            <span class="glyphicon glyphicon-edit"></span> Edit
          </button>
          <button class="delete-modal btn btn-danger" id="delete"
          data-team_id="{{$team->team_id}}" data-team_title="{{$team->team_title}}">
          <span class="glyphicon glyphicon-trash"></span> Delete
        </button></td>
      </tr>
    @endforeach
  </table>
</div>

<!-- Modal form to add a item -->
<div id="addModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>

      <div class="modal-body">
        <form class="form-horizontal" role="form">
          <div class="form-group">
            <label class="control-label col-sm-2" for="team_pic">Picture:</label>
            <div class="col-sm-10">
              <input type="file" class="form-control" id="pic_add" autofocus>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-2" for="team_title">Title:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="title_add" autofocus>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-2" for="team_span_desc">Description:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="desc_add" autofocus>
            </div>
          </div>

        </form>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-success add" data-dismiss="modal">
          <span id="" class='glyphicon glyphicon-check'></span> Add
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class='glyphicon glyphicon-remove'></span> Close
        </button>
      </div>
    </div>
  </div>
</div>
{{-- ============================================================= --}}


<!-- Modal form to edit a team -->
<div id="editModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>

      <div class="modal-body">
        <form class="form-horizontal" role="form">
          <div class="form-group">
            <label class="control-label col-sm-2" for="id">ID:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="id_edit" disabled>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="pic">Picture:</label>
            <div class="col-sm-10">
              <input type="file" class="form-control" id="pic_edit" autofocus>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="title">Title:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="title_edit" autofocus>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="description">Description:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="desc_edit" autofocus>
            </div>
          </div>
        </form>

        <div class="modal-footer">
          <button type="button" class="btn btn-success edit" data-dismiss="modal">
            <span id="" class='glyphicon glyphicon-check'></span> Edit
          </button>
          <button type="button" class="btn btn-warning" data-dismiss="modal">
            <span class='glyphicon glyphicon-remove'></span> Close
          </button>
        </div>

      </div>
    </div>
  </div>
</div>


<!-- Modal form to delete a form -->
<div id="deleteModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <h3 class="text-center">Are you sure you want to delete the following post?</h3>
        <br />
        <form class="form-horizontal" role="form">
          <div class="form-group">
            <label class="control-label col-sm-2" for="id">ID:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="id_delete" disabled>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="title">Title:</label>
            <div class="col-sm-10">
              <input type="name" class="form-control" id="title_delete" disabled>
            </div>
          </div>
        </form>
        <div class="modal-footer">
          <button type="button" class="btn actionBtn" data-dismiss="modal">
            <span id="footer_action_button" class='glyphicon glyphicon-trash'></span> Delete
          </button>
          <button type="button" class="btn btn-warning" data-dismiss="modal">
            <span class='glyphicon glyphicon-remove'></span> Close
          </button>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('js-links')
  @include('admin.contents.dashboard.js-links')
  {{-- <script src="{{ asset('custom/js/app.js') }}"></script> --}}
  <script src="{{ asset('custom/js/script.js') }}"></script>
@endsection
