@extends('admin.layouts.admin_masterpage')



@section('title')
  Manage Team
@endsection

@section('css-links')

 @include('admin.contents.dashboard.css-links')
 <!-- Fonts -->
 <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

 <!-- Styles -->
 <link rel="stylesheet" href="{{ asset('custom/css/app.css') }}">
 <link rel="stylesheet" href="{{ asset('custom/css/master.css') }}">

@endsection



@section('content')

  <nav class="navbar navbar-default navbar-fixed-top">
    <ul class="nav navbar-nav">
    <li><a href="http://justlaravel.com/">justlaravel.com</a></li>
    <li><a href="http://justlaravel.com/demos/">Demos home</a></li>
    </ul>
  </nav>
  <br><br><br><br>
  <div class="container">
    <div class="form-group row add">
      <div class="col-md-8">
        <input type="text" class="form-control" id="name" name="name"
          placeholder="Enter some name" required>
        <p class="error text-center alert alert-danger hidden"></p>
      </div>
      <div class="col-md-4">
        <button class="btn btn-primary" type="submit" id="add">
          <span class="glyphicon glyphicon-plus"></span> ADD
        </button>
      </div>
    </div>
     {{ csrf_field() }}
  <div class="table-responsive text-center">
    <table class="table table-borderless" id="table">
      <thead>
        <tr>
          <th class="text-center">S.N.</th>
          <th class="text-center">Team Picture</th>
          <th class="text-center">Team Title</th>
          <th class="text-center">Team Description</th>
        </tr>
      </thead>
      @foreach($team as $item)
      <tr class="item{{$item->team_id}}">
        <td>{{$item->team_id}}</td>
        <td>{{$item->team_pic}}</td>
        <td>{{$item->team_title}}</td>
        <td>{{$item->team_span_desc}}</td>
        <td><button class="edit-modal btn btn-info" data-team_id="{{$item->team_id}}"
            data-team_pic="{{$item->team_pic}}" data-team_title="{{$item->team_title}}" data-team_span_desc="{{$item->team_span_desc}}">
            <span class="glyphicon glyphicon-edit"></span> Edit
          </button>
          <button class="delete-modal btn btn-danger"
            data-team_id="{{$item->team_id}}" data-team_title="{{$item->team_title}}">
            <span class="glyphicon glyphicon-trash"></span> Delete
          </button></td>
      </tr>
      @endforeach
    </table>
  </div>
</div>


  <!-- Modal form to add a item -->


<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>

      <div class="modal-body">

        <form class="form-horizontal" role="form">

            <div class="form-group">
              <label class="control-label col-sm-2" for="id">ID:</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="tid" disabled>
                </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="pic">Picture:</label>
                <div class="col-sm-10">
                  <input type="file" class="form-control" id="tpic" disabled>
                </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="title">Title:</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="ttitle" disabled>
                </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="description">Description:</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="tdesc">
              </div>
            </div>

        </form>
        <div class="deleteContent">
          Are you Sure you want to delete <span class="dtitle"></span> ? <span
            class="hidden did"></span>
        </div>


        <div class="modal-footer">
          <button type="button" class="btn actionBtn" data-dismiss="modal">
            <span id="footer_action_button" class='glyphicon'> </span>
          </button>
          <button type="button" class="btn btn-warning" data-dismiss="modal">
            <span class='glyphicon glyphicon-remove'></span> Close
          </button>

      </div>
    </div>
  </div>
  </div>
  <script src="{{ asset('custom/js/app.js') }}"></script>
  <script src="{{ asset('custom/js/script.js') }}"></script>

      @endsection

      @section('js-links')
          @include('admin.contents.dashboard.js-links')
      @endsection
