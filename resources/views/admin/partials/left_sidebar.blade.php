<div class="left main-sidebar">

  <div class="sidebar-inner leftscroll">

    <div id="sidebar-menu">

    <ul>

        <li class="submenu">
            <a class="active" ><i class="fa fa-fw fa-bars"></i><span> Dashboard </span> </a>

        </li>

        <!-- <li class="submenu">
                      <a href="#"><i class="fa fa-user-secret"></i><span> User Roles </span> </a>
        </li> -->

        <li class="submenu">
            <a href="#" class="aboutus-button"><i class="fa fa-fw fa-user"></i> <span> About us </span></a>

        </li>

        <li class="submenu">
            <a href="{{ route('admin.contents.dashboard.portfolio') }}" class="portfolio"><i class="fa fa-briefcase"></i><span> Portfolio </span> </a>

        </li>

        <li class="submenu">
            <a href="#" class="services-button"><i class="fa fa-cogs" aria-hidden="true"></i><span> Services </span> </a>

        </li>

        <li class="submenu">
            <a href="{{ route('admin.contents.dashboard.team') }}" class="team"><i class="fa fa-users"></i><span> Team </span> </a>

        </li>

          <div class="clearfix"></div>

    </div>

    <div class="clearfix"></div>

  </div>

</div>
