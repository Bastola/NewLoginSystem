<?php



Route::get('/', function () {
    return view('viewer.view');
 });
//
// Auth::routes();
//
// Route::get('/home', 'HomeController@index')->name('home');

// Before Login routes
Route::prefix('admin')->group(function(){
  Route::get('/',function(){
    return redirect()->route('admin.login');
  });
  Route::get('login','Auth\LoginController@showLoginForm')->name('admin.login');
  Route::post('login','Auth\LoginController@login');
  Route::post('logout','Auth\LoginController@logout')->name('admin.logout');
});

// After Login routes
Route::group(['prefix'=>'admin','middleware'=>'auth'], function(){
  Route::get('/dashboard','Admin\DashboardController@index')->name('admin.dashboard.index');
  Route::get('portfolio', 'Admin\PortfolioController@index')->name('admin.contents.dashboard.portfolio');
// Route::post('portfolio', 'Admin\portfolioController@upload');
// Route::delete('portfolio/{id}', 'Admin\PortfolioController@destroy');


Route::get('team', 'Admin\TeamController@readTeam')->name('admin.contents.dashboard.team');
Route::post('addTeam', 'Admin\TeamController@addTeam');
Route::post('editTeam/{}', 'Admin\TeamController@editTeam');
Route::post('deleteTeam', 'Admin\TeamController@deleteTeam');


});
